﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MakeViewPage
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if(DialogResult.OK == dialog.ShowDialog())
            {
                txtPath.Text = dialog.SelectedPath;
            }
        }

        private void btnWork_Click(object sender, EventArgs e)
        {
            if (checkValidPath(txtPath.Text))
            {
                List<String> fileNames = readAllList(txtPath.Text);
                using(StreamWriter file = new StreamWriter(txtPath.Text +"\\test.html"))
                {
                    file.WriteLine("<meta charset=\"UTF-8\">");
                    foreach(string fileName in fileNames)
                    {
                        file.WriteLine("<img src=\"" + fileName + "\"><br>");
                    }
                }
            }
        }

        private bool checkValidPath(string path)
        {
            try
            {
                String goodPath = Path.GetFileName(path);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.StackTrace);
                return false;
            }
            return true;
        }

        private List<String> readAllList(string path)
        {
            List<String> resultList = new List<string>();
            DirectoryInfo di = new DirectoryInfo(path);
            foreach(FileInfo fi in di.GetFiles())
            {
                if(fi.Extension.ToLower().CompareTo(".jpg") == 0
                    || fi.Extension.ToLower().CompareTo(".png") == 0
                    ||fi.Extension.ToLower().CompareTo(".bmp") == 0
                    || fi.Extension.ToLower().CompareTo(".gif") == 0
                    )
                {
                    resultList.Add(fi.FullName);
                }
            }
            return resultList;
        }
    }
}
